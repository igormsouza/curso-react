//var React = require('react');
import React from 'react';

/*var Text = require('react-native').Text;
var View = require('react-native').View;
var Button = require('react-native').Button;
var AppRegistry = require('react-native').AppRegistry;*/
//var {Text, View, Button, AppRegistry} = require('react-native');//desctructuring assignment
import { Text, View, Button, AppRegistry } from 'react-native';

const geraNumeroAleatorio = () => {
    let numero = Math.random();
    numero *= 10;
    numero = Math.floor(numero);
    alert('Número: '+numero);
};

const App = ()=>{
    return (
        <View>
            <Text>Gerador de números randômicos</Text>
            <Button
                title="Gerar um número randômico"
                onPress={geraNumeroAleatorio}
            />
        </View>
        
    );
};

AppRegistry.registerComponent('app2', ()=>{return App});