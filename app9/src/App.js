import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import Routes from './Routes';
import reducers from './reducers';


export default class App extends Component {
    componentWillMount() {
        var config = {
            apiKey: "AIzaSyBEdd72ruiYQahG-agrok4Eu_J7gwDVVCg",
            authDomain: "whatsapp-clone-havok.firebaseapp.com",
            databaseURL: "https://whatsapp-clone-havok.firebaseio.com",
            projectId: "whatsapp-clone-havok",
            storageBucket: "whatsapp-clone-havok.appspot.com",
            messagingSenderId: "796840532756"
        };
        firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
                <Routes />
            </Provider>
        );
    }
}