import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import b64 from 'base-64';
import { 
    MODIFICA_EMAIL,
    MODIFICA_SENHA,
    MODIFICA_NOME,
    CADASTRO_USUARIO_ERRO,
    CADASTRO_USUARIO_SUCESSO,
    LOGIN_USUARIO_ERRO,
    LOGIN_USUARIO_SUCESSO,
    LOGIN_EM_ANDAMENTO,
    CADASTRO_EM_ANDAMENTO
} from './types';

export const modificaEmail = (texto) => {
    return {
        type: MODIFICA_EMAIL,
        payload: texto
    };
};

export const modificaSenha = (texto) => {
    return {
        type: MODIFICA_SENHA,
        payload: texto
    };
};

export const modificaNome = (texto) => {
    return {
        type: MODIFICA_NOME,
        payload: texto
    };
};

export const cadastraUsuario = (params) => {
    let email = params.email;
    let nome = params.nome;
    let senha = params.senha;
    return dispatch => {

        dispatch({ type: CADASTRO_EM_ANDAMENTO });

        firebase.auth()
        .createUserWithEmailAndPassword(email, senha)
        .then(user => {
            let emailB64 = b64.encode(email);
            firebase.database()
                .ref('/contatos/'+emailB64)
                .push({ nome })
                .then(value => cadastroUsuarioSucesso(dispatch));
            
        }).catch(error => cadastroUsuarioErro(error, dispatch));
    };
};

const cadastroUsuarioSucesso = (dispatch) => {
    dispatch({ type: CADASTRO_USUARIO_SUCESSO });
    Actions.boasvindas();
}

const cadastroUsuarioErro = (error, dispatch) => {
    let msg;
    switch (error.code) {
        case 'auth/email-already-in-use':
            msg = 'E-mail já está sendo usado.'
            break;
        case 'auth/invalid-email':
            msg = 'E-mail inválido!';
            break;
        case 'auth/operation-not-allowed':
            msg = 'E-mail desativado.';
            break;
        case 'auth/weak-password':
            msg = 'Senha deve conter no mínimo 6 caracteres';
            break;
        default:
            msg = '';
    }
    dispatch ( { type: CADASTRO_USUARIO_ERRO, payload: msg } );
}

export const autenticaUsuario = (email, senha) => {
    return dispatch => {

        dispatch({ type: LOGIN_EM_ANDAMENTO });

        firebase.auth().signInWithEmailAndPassword(email, senha)
        .then(value => loginUsuarioSucesso(dispatch))
        .catch(error => loginUsuarioerro(error, dispatch));
    }
}
const loginUsuarioSucesso = (dispatch) => {
    dispatch( {type: LOGIN_USUARIO_SUCESSO} );
    Actions.principal();
}
const loginUsuarioerro = (erro, dispatch) => {
    let msg = '';
    switch (erro.code) {
        case 'auth/invalid-email':
            msg = 'E-mail inválido.';
            break;
        case 'auth/user-disabled':
            msg = 'E-mail informado foi desabilitado.';
            break;
        case 'auth/user-not-found':
            msg = 'E-mail informado não foi encontrado.';
            break;
        case 'auth/wrong-password':
            msg = 'E-mail ou senha incorreta.';
            break;
        default:
            msg = '';
    }
    dispatch( {type: LOGIN_USUARIO_ERRO, payload: msg} );
}