import b64 from 'base-64';
import firebase from 'firebase';
import _ from 'lodash';

import {
    ADICIONA_CONTATO_EMAIL,
    ADICIONA_CONTATO,
    ADICIONA_CONTATO_ERRO,
    ADICIONA_CONTATO_SUCESSO,
    ADD_EM_ANDAMENTO,
    LISTA_CONTATO_USUARIO,
    MODIFICA_MENSAGEM,
    ENVIA_MENSAGEM,
    ENVIA_MENSAGEM_SUCESSO,
    ENVIA_MENSAGEM_ERRO,
    LISTA_CONVERSA_USUARIO,
    LISTA_CONVERSAS_USUARIO,
    LISTA_CONVERSAS_USUARIO_VAZIO
} from './types';

export const adicionaContatoEmail = (texto) => {
    return {
        type: ADICIONA_CONTATO_EMAIL,
        payload: texto
    };
};

export const adicionaContato = (email) => {
    return dispatch => {
        let emailB64 = b64.encode(email);

        dispatch({ type: ADD_EM_ANDAMENTO });

        firebase.database()
            .ref('/contatos/'+emailB64)
            .once('value')
            .then(snapshot => {
                if (snapshot.val()) {
                    //usando lodash
                    const dadosUsuario = _.first(_.values(snapshot.val()));

                    const { currentUser } = firebase.auth();
                    
                    let emailCurrentB64 = b64.encode(currentUser.email);
                    //não vou inserir o nome nessa tabela conforme o curso, pois fere as regras de normalizacao
                    firebase.database()
                        .ref('/usuario_contatos/'+emailCurrentB64)
                        .push({ email: email})
                        .then(() => adicionaContatoSucesso(dispatch))
                        .catch(
                            (error) => adicionaContatoErro(
                                'Ocorreu um erro ao adicionar o e-mail informado como contato.', dispatch
                            )
                        );
                } else {
                    adicionaContatoErro(
                        'E-mail informado não corresponde a um usuário válido.',
                        dispatch
                    );
                }
            }).catch((error)=>{
                adicionaContatoErro(
                    'Ocorreu um erro ao pesquisar o contato informado. Erro: ',
                    dispatch
                );
            });
    }
};

const adicionaContatoSucesso = (dispatch) => {
    dispatch({ type: ADICIONA_CONTATO_SUCESSO});
    alert('Contato adicionado com sucesso!');
}

const adicionaContatoErro = (error, dispatch) => {
    dispatch(
        {
            type: ADICIONA_CONTATO_ERRO, 
            payload: error
        }
    );
}

export const contatosUsuariosFetch = () => {
    const { currentUser } = firebase.auth();
    return dispatch => {
        let emailUsuarioB64 = b64.encode( currentUser.email );
        firebase.database().ref('/usuario_contatos/'+emailUsuarioB64)
            .on('value', snapshot => {
                dispatch({ type: LISTA_CONTATO_USUARIO, payload: snapshot.val() })
            });
    }
}

export const modificaMensagem = texto => {
    return ({
        type: MODIFICA_MENSAGEM,
        payload: texto
    })
}

export const enviaMensagem = (mensagem, contatoEmail) => {
    return dispatch => {

        let { currentUser } = firebase.auth();
        let currentEmail64 = b64.encode(currentUser.email);
        contatoEmail64 = b64.encode(contatoEmail);

        dispatch ({
            type: ENVIA_MENSAGEM
        });
        
        //insere mensagem para usuario autenticado
        firebase.database().ref(`/mensagens/${currentEmail64}/${contatoEmail64}`)
            .push({ mensagem, tipo: 'e' })
            .then(()=>{
                firebase.database().ref(`/mensagens/${contatoEmail64}/${currentEmail64}`)
                .push({ mensagem, tipo: 'r'})
                .then( () => enviaMensagemSucesso(dispatch) )
                .catch( error => enviaMensagemErro(error, dispatch) );
            })
            .then(() => { //cabecalho de conversa do usuario autenticado
                firebase.database().ref(`/usuario_conversas/${currentEmail64}/${contatoEmail64}`)
                    .set({ email: contatoEmail });
                    
            })
            .then(() => { //cabecalho de conversa do contato
                firebase.database().ref(`/usuario_conversas/${contatoEmail64}/${currentEmail64}`)
                    .set({ email: currentUser.email });
            })
            .catch( error => enviaMensagemErro(error, dispatch) );
    }
}

const enviaMensagemSucesso = dispatch => {
    dispatch({
        type: ENVIA_MENSAGEM_SUCESSO
    })
}

const enviaMensagemErro = (erro, dispatch) => {
    dispatch({
        type: ENVIA_MENSAGEM_ERRO
    })
}

export const conversaUsuarioFetch = contatoEmail => {
    const { currentUser } = firebase.auth();

    let usuarioEmail64 = b64.encode(currentUser.email);
    let contatoEmail64 = b64.encode(contatoEmail);

    return dispatch => {
        firebase.database().ref(`/mensagens/${usuarioEmail64}/${contatoEmail64}`)
            .on('value', snapshot => {
                dispatch({ type: LISTA_CONVERSA_USUARIO, payload: snapshot.val() });
            });
    }
}


export const conversasUsuarioFetch = () => {
    const { currentUser } = firebase.auth();

    return dispatch => {
        let usuarioEmailB64 = b64.encode(currentUser.email);

        firebase.database().ref(`/usuario_conversas/${usuarioEmailB64}`)
            .on("value", snapshot => {
                dispatch({ type: LISTA_CONVERSAS_USUARIO, payload: snapshot.val() })
            })
    }
}

/*
export const conversasUsuarioFetch = () => {
    const { currentUser } = firebase.auth();
    let usuarioEmail64 = b64.encode(currentUser.email);
    
    return dispatch => {
        firebase.database().ref(`/usuario_conversas/${usuarioEmail64}`)
            .on('value', snapshot => {
                if(snapshot.val()){
                    dispatch({ type: LISTA_CONVERSAS_USUARIO, payload: snapshot.val() });
                }else{
                    dispatch({ type: LISTA_CONVERSAS_USUARIO_VAZIO });
                }
            })
            .catch(error=>console.log(error));
    }
}*/
