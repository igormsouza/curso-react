import React, { Component } from 'react';
import { View, Text, ListView, StyleSheet, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import ListaContatosReducer from '../reducers/ListaContatosReducer';
import { contatosUsuariosFetch } from '../actions/AppActions';

const Styles = StyleSheet.create({
    linhaContato: {
        flex: 1,
        padding: 20,
        borderBottomWidth: 1,
        borderColor: "#ccc"
    }
});

class Contatos extends Component {

    componentWillMount() {
        this.props.contatosUsuariosFetch();

        this.criaFonteDeDados( this.props.contatos );
    }

    componentWillReceiveProps(nextProps) {
        let contatos = nextProps.contatos;
        this.criaFonteDeDados( contatos );
    }

    criaFonteDeDados( contatos ) {
        //cria um datasource para ser usado na listview
        //define qual o critério para a listview saber quando uma linha mudou
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) =>  r1 !== r2 });
        
        this.fonteDeDados = ds.cloneWithRows( contatos );
    }
    
    abreConversa(params) {
        Actions.conversa(params);
    }
    renderlinha(contato) {
        return (
            <TouchableHighlight underlayColor="transparent" onPress={()=>this.abreConversa({title: contato.email, contatoEmail: contato.email})}> 
                <View style={Styles.linhaContato}>
                    <Text>{contato.email}</Text>
                </View>
            </TouchableHighlight>
        );
    }
    render() {
        return (
            <ListView
                enableEmptySections
                dataSource={this.fonteDeDados}
                renderRow={ data => this.renderlinha(data) }
            />
        );
    }    
}

const mapStateToProps = state => {
    const contatos = _.map(state.ListaContatosReducer, (val, uid) => {
        return { ...val, uid };
    } );
    return {
        //contatos: state.ListaContatosReducer.contatos
        contatos
    }
}

export default connect(mapStateToProps, { contatosUsuariosFetch })(Contatos);