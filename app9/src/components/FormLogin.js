import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TextInput, 
    Button, 
    StyleSheet, 
    TouchableHighlight, 
    Image,
    ActivityIndicator
 } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { modificaEmail, modificaSenha, autenticaUsuario } from '../actions/AutenticacaoActions';

const Styles = StyleSheet.create({
    bg: {
        flex: 1,
        width: null
    },
    container: {
        flex: 1,
        padding: 10
    },
    topo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtTitulo: {
        fontSize: 25,
        color: '#fff'
    },
    meio: {
        flex: 2
    },
    inputForm: {
        fontSize: 20,
        height: 45
    },
    txtForm: {
        fontSize: 20,
        color: '#fff'
    },
    rodape: {
        flex: 2
    }, 
    erro: {
        color: '#ff0000', 
        fontSize: 18
    }
});

const bg_image = require('../imgs/bg.png');

class FormLogin extends Component {
    _autenticaUsuario() {
        const params = {email, senha} = this.props;
        this.props.autenticaUsuario(params.email, params.senha);
    }

    renderBtnAcessar() {
        if(this.props.loadingLogin) {
            return (
                <ActivityIndicator size="large" />
            );
        }
        return (
            <Button color="#115e54" title="Acessar" onPress={()=>this._autenticaUsuario()}/>
        );
    }
    render() {
        return (
            <Image style={Styles.bg} source={bg_image}>
                <View style={Styles.container}>
                    <View style={Styles.topo}>
                        <Text style={Styles.txtTitulo}>WhatsApp Clone</Text>
                    </View>
                    <View style={Styles.meio}>
                        <TextInput 
                            value={this.props.email} 
                            style={Styles.inputForm} 
                            placeholderTextColor="#fff" 
                            placeholder="E-mail" 
                            onChangeText={texto => this.props.modificaEmail(texto)} />
                        <TextInput 
                            value={this.props.senha} 
                            style={Styles.inputForm} 
                            placeholderTextColor="#fff" 
                            placeholder="Senha"
                            secureTextEntry
                            onChangeText={texto => this.props.modificaSenha(texto)} />
                        <Text style={Styles.erro}>{this.props.erroLogin}</Text>

                        <TouchableHighlight 
                            onPress={Actions.cadastro} 
                            activeOpacity={0.3} 
                            underlayColor="transparent">
                            <Text style={Styles.txtForm}>Ainda não tem cadastro? Cadastre-se</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={Styles.rodape}>
                        {
                            this.renderBtnAcessar()
                        }
                    </View>
                </View>
            </Image>
        );
    }
}

const mapStateToProps = state => (
    {
        email: state.AutenticacaoReducer.email,
        senha: state.AutenticacaoReducer.senha,
        erroLogin: state.AutenticacaoReducer.erroLogin,
        loadingLogin: state.AutenticacaoReducer.loadingLogin
    }
);

export default connect(mapStateToProps, { modificaEmail, modificaSenha, autenticaUsuario })(FormLogin);
