import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image, 
    StyleSheet, 
    TextInput, 
    TouchableHighlight, 
    ListView 
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import { modificaMensagem, enviaMensagem, conversaUsuarioFetch }  from '../actions/AppActions';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#eee4dc'
    },
    viewMensagens: {
        flex: 1,
        padding: 10,
        marginBottom: 10,
        marginTop: 80
    },
    viewMensagem: {
        marginTop: 5,
        marginBottom: 5
    },
    txtMensagem: {
        fontSize: 18,
        color: '#000',
        backgroundColor: '#dbf5ba',
        elevation: 1,
        padding: 5,
        borderRadius: 5
    },
    viewMensagemEnviada: {
        alignItems: 'flex-end',
        marginLeft: 40
    },
    viewMensagemRecebida: {
        alignItems: 'flex-start',
        marginRight: 40
    },
    viewDigita: {
        height: 70,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        marginTop: 80
    },
    campoDigita: {
        flex: 4,
        backgroundColor: '#fff',
        fontSize: 18,
        height: 50,
        padding: 5,
        borderRadius: 3
    },
    btn_envia: {
        margin: 5
    }
});

const img_envia = require('../imgs/enviar_mensagem.png');

class Conversa extends Component {

    componentWillMount() {
        this.props.conversaUsuarioFetch(this.props.contatoEmail);
        this.criaFonteDeDados(this.props.conversas);
    }

    componentWillReceiveProps( nextProps ) {
        if(this.props.contatoEmail != nextProps.contatoEmail) {
            this.props.conversaUsuarioFetch(nextProps.contatoEmail);
        }
        this.criaFonteDeDados( nextProps.conversas );
    }

    criaFonteDeDados( conversas ) {
        //cria um datasource para ser usado na listview
        //define qual o critério para a listview saber quando uma linha mudou
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) =>  r1 !== r2 });
        
        this.fonteDeDados = ds.cloneWithRows( conversas );
    }

    _enviaMensagem() {
        const { mensagem, contatoEmail } = this.props;
        this.props.enviaMensagem( mensagem, contatoEmail );
    }

    renderLinha( conversa ) {
        if(conversa.tipo === 'e') {
            return (
                <View style={[Styles.viewMensagem, Styles.viewMensagemEnviada]}>
                    <Text style={Styles.txtMensagem}>{conversa.mensagem}</Text>
                </View>
            );
        }
        return (
            <View style={[Styles.viewMensagem, Styles.viewMensagemRecebida]}>
                <Text style={[Styles.txtMensagem, {backgroundColor: '#f7f7f7'}]}>{conversa.mensagem}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={Styles.container}>
                <View style={Styles.viewMensagens}>
                    <ListView 
                        enableEmptySections
                        dataSource={this.fonteDeDados}
                        renderRow={ data => this.renderLinha(data) }
                    />
                </View>
                <View style={Styles.viewDigita}>
                    <TextInput 
                        style={Styles.campoDigita} 
                        value={this.props.mensagem} 
                        onChangeText={texto => this.props.modificaMensagem(texto)} />
                    <TouchableHighlight onPress={()=>this._enviaMensagem()} underlayColor="transparent" >
                        <Image style={Styles.btn_envia} source={img_envia} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    const conversas = _.map(state.ListaConversaReducer, (val, uid)=>{
        return { ...val, uid };
    });

    return ({
        mensagem: state.AppReducer.mensagem,
        conversas
    });
}

export default connect(mapStateToProps, { modificaMensagem, enviaMensagem, conversaUsuarioFetch })(Conversa);