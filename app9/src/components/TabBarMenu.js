import React from 'react';
import { View, Text, StatusBar, Image, StyleSheet, TouchableHighlight } from 'react-native';
import { TabBar } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';

const icone = require('../imgs/adicionar-contato.png');

const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#115e54',
        elevation: 4,
        marginBottom: 6
    },
    titulo: {
        color: '#fff', 
        fontSize: 20, 
        marginLeft: 20
    },
    txtSair: {
        color: '#fff',
        fontSize: 18
    },
    viewStatusbar: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    viewTitulo: {
        height: 50,
        justifyContent: 'center'
    },
    viewIcones: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 20
    },
    viewAddContato: {
        alignItems:'center',
        width: 50, 
        marginRight: 20
    }
});

export default props => (
    <View style={Styles.container}>
        <StatusBar backgroundColor="#114d44" />
        <View style={Styles.viewStatusbar}>
            <View style={Styles.viewTitulo}>
                <Text style={Styles.titulo}>Whatsapp Clone</Text>
            </View>
            <View style={Styles.viewIcones}>
                <View style={Styles.viewAddContato}>
                    <TouchableHighlight underlayColor="#114d44" onPress={()=>Actions.addcontato()}>
                        <Image source={icone}/>
                    </TouchableHighlight>
                </View>
                <View >
                    <TouchableHighlight underlayColor="#114d44" onPress={()=>{
                            firebase
                                .auth()
                                .signOut()
                                .then(()=>Actions.login())
                                .catch(err=>alert('Ocorreu um erro ao deslogar'));
                        }}>
                        <Text style={Styles.txtSair}>Sair</Text>
                    </TouchableHighlight>
                </View>
            </View>
        </View>
        <TabBar {...props} style={{backgroundColor: "#115e54", elevation: 0}}/>
    </View>
)