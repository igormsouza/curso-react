import React, { Component } from 'react';
import { View, Text, ListView, TouchableHighlight, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import ListaConversasReducer from '../reducers/ListaConversasReducer';
import { conversasUsuarioFetch } from '../actions/AppActions';

const styles = StyleSheet.create({
    viewConversa: {
        padding: 10,
        borderBottomWidth: 1,
        borderColor: "#ccc"
    },
    txtConversa: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});

class Conversas extends Component {

    componentWillMount() {
        
        this.props.conversasUsuarioFetch();
        console.log(this.props.conversas);
        this.criaFonteDeDados( this.props.conversas );
    }

    componentWillReceiveProps( nextProps ) {
        this.criaFonteDeDados( nextProps.conversas );
        console.log("NEXT>"+nextProps.conversas);
    }

    criaFonteDeDados( conversas ) {
        //cria um datasource para ser usado na listview
        //define qual o critério para a listview saber quando uma linha mudou
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) =>  r1 !== r2 });
        
        this.fonteDeDados = ds.cloneWithRows( conversas );
    }
    
    renderLinha(conversas) {
        
        return (
            <TouchableHighlight onPress={()=>Actions.conversa({ title: conversas.email, contatoEmail: conversas.email })}>
                <View style={styles.viewConversa}>
                    <Text style={styles.txtConversa}>{conversas.email}</Text>
                </View>
            </TouchableHighlight>
        );
    }
    
    render() {
        
            return (
                <View >
                    <ListView 
                        enableEmptySections
                        dataSource={this.fonteDeDados}
                        renderRow={ data => this.renderLinha(data) }
                    />
                </View>
            );
    /*
        return (
            <View>
                <Text>Nenhuma conversa ainda</Text>
            </View>
        );*/
        
    }
    /*
    render() {
        return (
            <View>
                <Text>Testes</Text>
            </View>
        );
    }*/
}

const mapStateToProps = state => {
    const conversas = _.map(state.ListaConversasReducer, (val, uid)=>{
        return { ...val, uid };
    });

    return ({
        conversas
    });
}

export default connect ( mapStateToProps, { conversasUsuarioFetch }) (Conversas);

