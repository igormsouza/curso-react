import React from 'react';
import { Text, Image, View, Button, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');
const bg_image = require('../imgs/bg.png');

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15
    },
    viewLogo: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtLogo: {
        color: '#fff',
        fontSize: 18
    },
    viewBtn: {
        flex: 1
    }
});
export default props => (
    <Image style={{flex: 1, width: null}} source={bg_image}>
        <View style={Styles.container}>
            <View style={Styles.viewLogo}>
                <Text style={Styles.txtLogo}> Seja Bem-Vindo</Text>
                <Image source={logo}/>
            </View>
            <View style={Styles.viewBtn}>
                <Button title="Fazer login" onPress={()=>{Actions.login()}} />
            </View>
        </View>
    </Image>
);