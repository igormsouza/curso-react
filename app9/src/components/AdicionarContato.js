import React, { Component } from 'react';
import { View, Text, TextInput, Button, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { 
    adicionaContatoEmail,
    adicionaContato
} from '../actions/AppActions';

const Styles = StyleSheet.create({
    erro: {
        color: '#ff0000', 
        fontSize: 18
    }
});

class AdicionarContato extends Component {

    _adicionaContato() {
        this.props.adicionaContato(this.props.email);
    }

    renderBtnAdicionar() {
        if(this.props.loadingAdd){
            return (
                <ActivityIndicator size="large" />
            );
        }
        return (
            <Button title="Adicionar" onPress={ () => this._adicionaContato() }/>
        );
    }

    render(){ 
        return (
            <View style={{flex: 1, justifyContent: 'center', padding: 20}}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <TextInput 
                        placeholder="E-mail" 
                        style={{fontSize: 20, height: 45}} 
                        onChangeText={ (texto)=> this.props.adicionaContatoEmail(texto) }
                        value={this.props.email} 
                        />
                    <Text style={Styles.erro}>{this.props.cadastro_resultado_txt_erro}</Text>
                </View>
                <View style={{flex: 1}}>
                    {this.renderBtnAdicionar()}
                </View>
                
            </View>
        );
    }
}

const mapStateToProps = state => (
    {
        email: state.AppReducer.email,
        cadastro_resultado_txt_erro: state.AppReducer.cadastro_resultado_txt_erro,
        loadingAdd: state.AppReducer.loadingAdd
    }
);
export default connect(
    mapStateToProps, 
    { 
        adicionaContatoEmail,
        adicionaContato
    }
)(AdicionarContato);