import React, { Component } from 'react';
import { 
    View, 
    TextInput, 
    Button, 
    StyleSheet, 
    Image, 
    Text,
    ActivityIndicator 
} from 'react-native';
import { connect } from 'react-redux';
import { 
    modificaNome, 
    modificaEmail, 
    modificaSenha,
    cadastraUsuario
} from '../actions/AutenticacaoActions';

const Styles = StyleSheet.create({
    bg: {
        flex: 1,
        width: null
    },
    container: {
        flex: 1, 
        padding: 10
    },
    viewInputs: {
        flex: 4,
        justifyContent: 'center'
    },
    inputs: {
        fontSize: 20, 
        height: 45
    },
    viewBtn: {
        flex: 1
    },
    erro: {
        color: '#ff0000', 
        fontSize: 18
    }
});
const bg_image = require('../imgs/bg.png');

class FormCadastro extends Component {
    _cadastraUsuario() {
        let params = { nome, email, senha } = this.props;
        this.props.cadastraUsuario(params);
    }
    renderBtnCadastrar() {
        if(this.props.loadingCadastro){
            return (
                <ActivityIndicator size="large" />
            );
        }
        return (
            <Button title="Cadastrar" color="#115e54" onPress={() => {this._cadastraUsuario()}} />
        );
    }
    render() {
        return (
            <Image style={Styles.bg} source={bg_image}>
                <View style={Styles.container}>
                    <View style={Styles.viewInputs}>
                        <TextInput 
                            value={this.props.nome} 
                            style={Styles.inputs} 
                            placeholder="Nome" 
                            onChangeText={ (texto) => (this.props.modificaNome(texto)) }  
                            placeholderTextColor="#fff" />
                        <TextInput 
                            value={this.props.email} 
                            style={Styles.inputs} 
                            placeholder="E-mail" 
                            onChangeText={ (texto) => (this.props.modificaEmail(texto)) }  
                            placeholderTextColor="#fff" />
                        <TextInput 
                            secureTextEntry 
                            value={this.props.senha} 
                            style={Styles.inputs} 
                            placeholder="Senha" 
                            onChangeText={ (texto) => (this.props.modificaSenha(texto)) }  
                            placeholderTextColor="#fff" />
                        <Text style={Styles.erro}>{this.props.erroCadastro}</Text>
                    </View>
                    
                    <View style={Styles.viewBtn}>
                        { this.renderBtnCadastrar() }
                    </View>
                </View>
            </Image>
        );
    }
}

const mapStateToProps = state => (
    {
        nome: state.AutenticacaoReducer.nome,
        email: state.AutenticacaoReducer.email,
        senha: state.AutenticacaoReducer.senha,
        erroCadastro: state.AutenticacaoReducer.erroCadastro,
        loadingCadastro: state.AutenticacaoReducer.loadingCadastro
    }
);

export default connect(
    mapStateToProps, 
    { 
        modificaNome, 
        modificaEmail, 
        modificaSenha,
        cadastraUsuario
    }
)(FormCadastro);