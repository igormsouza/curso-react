import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import FormLogin from './components/FormLogin';
import FormCadastro from './components/FormCadastro';
import BoasVindas from './components/BoasVindas';
import Principal from './components/Principal';
import AdicionarContato from './components/AdicionarContato';
import Conversa from './components/Conversa';

const Routes = () => (
    <Router 
        navigationBarStyle={{backgroundColor: "#115e54"}} 
        titleStyle={{color: '#fff'}} >
        <Scene key='cadastro' component={FormCadastro} title="Cadastro" hideNavBar={false} />
        <Scene key='login' component={FormLogin} initial title="" hideNavBar />
        <Scene key='boasvindas' component={BoasVindas} title="" hideNavBar />
        <Scene key='principal' component={Principal}  title="" hideNavBar />
        <Scene key='addcontato' component={AdicionarContato} title="Adicionar contato" hideNavBar={false} />
        <Scene key='conversa' component={Conversa} title="Conversa" hideNavBar={false} />
    </Router>
);

export default Routes;
