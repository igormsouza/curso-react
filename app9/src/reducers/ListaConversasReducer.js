import { 
    LISTA_CONVERSAS_USUARIO, LISTA_CONVERSAS_USUARIO_VAZIO
} from '../actions/types';

const INITIAL_STATE = {
    conversas: []
}

export default ( state = INITIAL_STATE, action ) => {
    
    switch (action.type) {
        case LISTA_CONVERSAS_USUARIO:
            return action.payload;
        case LISTA_CONVERSAS_USUARIO_VAZIO:
            return state.conversas;
        default: 
            return state;
    }
}