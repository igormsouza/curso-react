import { 
    ADICIONA_CONTATO_EMAIL,
    ADICIONA_CONTATO,
    ADICIONA_CONTATO_ERRO,
    ADICIONA_CONTATO_SUCESSO,
    ADD_EM_ANDAMENTO,
    MODIFICA_MENSAGEM,
    ENVIA_MENSAGEM
} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    cadastro_resultado_txt_erro: '',
    loadingAdd: false,
    mensagem: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADICIONA_CONTATO_EMAIL:
            return {...state, email: action.payload }
        case ADICIONA_CONTATO:
            return {...state, loadingAdd: true }
        case ADICIONA_CONTATO_ERRO:
            return {...state, cadastro_resultado_txt_erro: action.payload, loadingAdd: false }
        case ADICIONA_CONTATO_SUCESSO:
            return {...state, email: '', loadingAdd: false }
        case ADD_EM_ANDAMENTO:
            return {...state, loadingAdd: true }
        case MODIFICA_MENSAGEM:
            return {...state, mensagem: action.payload }
        case ENVIA_MENSAGEM:
            return {...state, mensagem: ''}
        default:
            return state;
    }
}
