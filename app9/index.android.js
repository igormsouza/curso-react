import React from 'react';
import {
  AppRegistry
} from 'react-native';

import App from './src/App';

const app9 = props => (<App />);

AppRegistry.registerComponent('app9', () => app9);
