import React from 'react';
import {Text, View, Button, AppRegistry, Image, TouchableOpacity, Alert} from 'react-native';

const Estilos = {
  principal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  botao: {
      backgroundColor: '#538530',
      paddingVertical: 10,
      paddingHorizontal: 40,
      marginTop: 20
  },
  textoBotao: {
      color: '#fff',
      fontSize: 16,
      fontWeight: 'bold'
  }
};

const App = () => {
  let {principal, botao, textoBotao} = Estilos;
  let mostraFrase = () => {
      let num = Math.floor((Math.random())*10);
      let frases = Array();
      frases[0] = 'A vitalidade é demonstrada não apenas pela persistência, mas pela capacidade de começar de novo. -F. Scott Fitzgerald';
      frases[1] = 'A coragem não é ausência do medo; é a persistência apesar do medo. -Desconhecido';
      frases[2] = 'O homem não teria alcançado o possível se, repetidas vezes, não tivesse tentado o impossível. -Max Webber';
      frases[3] = 'Só se pode alcançar um grande êxito quando nos mantemos fiéis a nós mesmos. -Friedrich Nietzsche';
      frases[4] = 'Todo mundo é capaz de sentir os sofrimentos de um amigo. Ver com agrado os seus êxitos exige uma natureza muito delicada. -Oscar Wilde';
      frases[5] = 'Creia em si, mas não duvide sempre dos outros. -Machado de Assis';
      frases[6] = 'As únicas grandes companhias que conseguirão ter êxito são aquelas que consideram os seus produtos obsoletos antes que os outros o façam. -Bill Gates';
      frases[7] = 'Estar decidido, acima de qualquer coisa, é o segredo do êxito. -Henry Ford';
      frases[8] = 'Para obter êxito no mundo temos de parecer loucos mas sermos espertos. -Barão de Montesquieu';
      frases[9] = 'Nenhum mentiroso tem uma memória suficientemente boa para ser um mentiroso de êxito. -Abraham Lincoln';
      
      Alert.alert('', frases[num]);
  };

  return (
    <View style={principal}>
      <Image source={require('./imgs/logo.png')}/>
      <TouchableOpacity style={botao} onPress={mostraFrase}>
          <Text style={textoBotao}>Nova frase</Text>
      </TouchableOpacity>
    </View>
  );
};

AppRegistry.registerComponent('app3', ()=> App);