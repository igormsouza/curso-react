import React from 'react';
import {Text, View, Button, AppRegistry, TouchableOpacity, Image} from 'react-native';
/*
const Estilos = {
  estiloTexto: {
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
    fontStyle: 'italic',
    fontWeight: 'bold',
    textDecorationLine: 'underline line-through',
    backgroundColor: '#08509B',
    //height: 80,
    //width: 300,
    padding: 20,
    //paddingTop: 20,
    //paddingLeft: 20,
    //paddingBottom: 20,
    //paddingRight: 20,
    margin: 20
    //marginHorizontal: 10,
    //marginVertical: 10,
    //Shadows won't work on android
    //shadowColor: '#000',
    //shadowOffset: {width:0, height: 15},
    //shadowOpacity: 1,
    //elevation: 2,
    
  }
};*/

/*Flexbox
const Estilos = {
  principal: {
    flex: 1,
    backgroundColor: 'cornflowerblue'
  },
  topo: {
    flex: 1,
    backgroundColor: 'brown'
  },
  conteudo: {
    flex: 3,
    backgroundColor: 'yellowgreen'
  },
  rodape: {
    flex: 1,
    backgroundColor: 'orangered'
  }
}; */

const Estilos = {
  principal: {
    paddingTop: 40
  },
  botao: {
    backgroundColor: '#48bbec',
    padding: 10,
    borderColor: '#1d8eb8',
    borderWidth: 3,
    borderRadius: 8
  },
  textoBotao: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  imagem: {
    justifyContent: 'center'
  }
};

const App = () => {
  let alerta = () => {
    alert('Hello');
  };
  let {principal, botao, textoBotao, imagem} = Estilos;
  
  return (
    <View style={principal}>
      <TouchableOpacity style={botao} onPress={alerta}>
        <Text style={textoBotao}>Clique aqui</Text>
        
      </TouchableOpacity>
      <Image style={imagem} source={ require('./imgs/uvas.png') }>
        <Text style={textoBotao} >Legenda para a foto</Text>
      </Image>
    </View>
  );
};

AppRegistry.registerComponent('app3', ()=> App);