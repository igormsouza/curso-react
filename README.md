# Comandos:

**Iniciar react**: `react-native start`

**Rodar app no android**: `react-native run-android`

**Mostrar dispositivos**: `adb devices`

**Listar emuladores**: `emulator -list-avds`

*Localização do emulador ~Android/Sdk/emulator*

**Iniciar emulador**: `emulator -avd nome_emulador`

**Direciona comandos para dispositivo conectado**: `adb reverse tcp:8081 tcp:8081`

**Abre menu dev no celular**: `adb shell input keyevent 82`

**Recarrega app**: `adb shell input keyevent 46 46`

**Gerar apk**: `gradlew assembleDebug`

**Instalar apk**: `adb install <nome.apk>`
*Apk gerado vai para android/app/build/outputs/apk*

**Inicilizar app**:`react-native init --version="0.44.0" <nome_app>`
