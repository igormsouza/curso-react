import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


const Estilos = StyleSheet.create({
  cenaPrincipal: {
    flex: 1,
    backgroundColor: '#61bd8c'
  }
});

export default class CenaOutrosJogos extends Component {
  render() {
    return (
      <View style={Estilos.cenaPrincipal}>
        <Text>Apresentadas informações sobre outros jogos do desenvolvedor.</Text>
      </View>
    );
  }
}


