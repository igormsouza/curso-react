import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


const Estilos = StyleSheet.create({
  cenaPrincipal: {
    flex: 1,
    backgroundColor: '#61bd8c'
  }
});

export default class CenaSobreJogo extends Component {
  render() {
    return (
      <View style={Estilos.cenaPrincipal}>
        <Text>Aqui podem ser apresentadas informações sobre o jogo</Text>
      </View>
    );
  }
}


