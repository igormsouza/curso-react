import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');
const btnJogar = require('../imgs/botao_jogar.png');
const btnSobre = require('../imgs/sobre_jogo.png');
const btnOutros = require('../imgs/outros_jogos.png');

const Estilos = StyleSheet.create({
  cenaPrincipal: {
    flex: 1,
    backgroundColor: '#61bd8c'
  },
  apresentacao: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rodape: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export default class cenaPrincipal extends Component {
  render() {
    return (
      <View style={Estilos.cenaPrincipal}>
        <View style={Estilos.apresentacao}>
          <Image source={logo} />
          <TouchableHighlight 
                onPress={()=>{
                    Actions.resultado();
                }}>
                <Image source={btnJogar} />
            </TouchableHighlight>
          
        </View>
        <View style={Estilos.rodape}>
            <TouchableHighlight 
                onPress={()=>{
                    Actions.sobrejogo();
                }}>
                <Image source={btnSobre} />
            </TouchableHighlight>
            <TouchableHighlight
                onPress={()=>{
                    Actions.outrosjogos();
                }}>
                <Image source={btnOutros} />
            </TouchableHighlight>
        </View>
      </View>
    );
  }
}
