
import React, { Component } from 'react';
import {
  ScrollView, Text, StyleSheet
} from 'react-native';

import Item from './Item';
import axios from 'axios';

class ListaItens extends Component {
    constructor(props) {
        super(props);
        this.state = { listaItens: []};
    }
    componentWillMount() {
        let url = 'http://faus.com.br/recursos/c/dmairr/api/itens.html';
        axios.get(url)
        .then( (response)=>{ this.setState({ listaItens: response.data }); } )
        .catch( () => { console.log('Erro ao recuperar os dados') } );
    }
    
    render() {
        return (
        <ScrollView style={ Estilos.lista }>
            { this.state.listaItens.map(item => (<Item key={item.titulo} item={item} />))}
        </ScrollView>
        );
    }
}


const Estilos = {
    lista: {
      
      backgroundColor: '#ddd'
    }
}

export default ListaItens;