import React, { Component } from 'react';
import {
  Text, View, Image, StyleSheet
} from 'react-native';

class Item extends Component {
  render() {
    return (
      <View style={Estilos.viewItem}>
        <View style={Estilos.viewImage}>
          <Image style={{height: 100, width: 100}} source={{ uri: this.props.item.foto }} />
        </View>
        <View style={Estilos.viewTexto}>
          <Text style={Estilos.txtTitulo}>{this.props.item.titulo}</Text>
          <Text style={Estilos.texto}>R$ {this.props.item.valor}</Text>
          <Text style={Estilos.texto}>{this.props.item.local_anuncio}</Text>
          <Text style={Estilos.texto}>Dt. Publicação: {this.props.item.data_publicacao}</Text>
        </View>
      </View>
    );
  }
}

const Estilos = {
  viewItem: {
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: '#999',
    borderRadius: 4,
    borderWidth: 0.5
  },
  viewImage: {
    marginRight: 10
  },
  viewTexto: {
    padding: 8,
    flex: 1
  },
  texto: {
    fontSize: 12
  },
  txtTitulo: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5
  }
}

export default Item;
