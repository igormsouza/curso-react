import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheContato = require('../imgs/detalhe_contato.png');

const Estilos = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    txtTitulo:{
        color: '#61bd8c',
        fontSize: 30,
        marginLeft: 10
    },
    viewTitulo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    viewContatos: {
        marginTop: 20,
        padding: 20
    },
    txtContatos: {
        fontSize: 16
    }
});

export default class CenaContatos extends Component {
  render() {
    return (
        <View style={Estilos.container}>
            <StatusBar backgroundColor={'#61bd8c'} />
            <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#61bd8c'/> 
            <View style={Estilos.viewTitulo}>
                <Image source={detalheContato} />
                <Text style={Estilos.txtTitulo}>Contatos</Text>
            </View>
            <View style={Estilos.viewContatos}>
                <Text style={Estilos.txtContatos}>TEL: (63) 3476-3630</Text>
                <Text style={Estilos.txtContatos}>CEL: (63) 98491-1532</Text>
                <Text style={Estilos.txtContatos}>E-MAIL: havok1305@gmail.com</Text>
            </View>
        </View>
    );
  }
}
