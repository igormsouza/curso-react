import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

const btnVoltar = require('../imgs/btn_voltar.png');
export default class BarraNavegacao extends Component {
  render() {
    if(this.props.voltar){
    return (
        <View style={[Estilos.barraTitulo, {backgroundColor: this.props.corDeFundo}]}>
            <TouchableHighlight 
                underlayColor={this.props.corDeFundo}
                activeOpacity={0.3}
                onPress={()=>{
                    this.props.navigator.pop();
                }}>
                <Image source={btnVoltar} />
            </TouchableHighlight>
            
            <Text style={Estilos.titulo}>ATM Consultoria</Text>
        </View>
    );
    }
    
    return (
    <View style={Estilos.barraTitulo}>
        <Text style={Estilos.titulo}>ATM Consultoria</Text>
    </View>
    );
  }
}

const Estilos = StyleSheet.create({
    barraTitulo: {
        backgroundColor: '#ccc',
        padding: 10,
        height: 60,
        flexDirection: 'row'
        
    },
    titulo: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        color: '#000'
    }
});