import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheEmpresa = require('../imgs/detalhe_empresa.png');

const Estilos = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    txtTitulo:{
        color: '#ec7148',
        fontSize: 30,
        marginLeft: 10
    },
    viewTitulo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    viewEmpresa: {
        marginTop: 20,
        padding: 20
    },
    txtEmpresa: {
        fontSize: 16
    }
});

export default class CenaEmpresa extends Component {
  render() {
    return (
        <View style={Estilos.container}>
            <StatusBar backgroundColor={'#ec7148'} />
            <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo={'#ec7148'}/> 
            <View style={Estilos.viewTitulo}>
                <Image source={detalheEmpresa} />
                <Text style={Estilos.txtTitulo}>A empresa</Text>
            </View>
            <View style={Estilos.viewEmpresa}>
                <Text style={Estilos.txtEmpresa}>A ATM consultoria está no mercado a mais de 20 anos ...</Text>
            </View>
        </View>
    );
  }
}
