import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheCliente = require('../imgs/detalhe_cliente.png');
const cliente1 = require('../imgs/cliente1.png');
const cliente2 = require('../imgs/cliente2.png');

const Estilos = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    txtTitulo:{
        color: '#b9c941',
        fontSize: 30,
        marginLeft: 10
    },
    viewTitulo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    viewCliente: {
        padding: 20,
        marginTop: 10
    },
    txtCliente: {
        fontSize: 16,
        marginLeft: 20
    }
});

export default class CenaClientes extends Component {
  render() {
    return (
        <View style={Estilos.container}>
            <StatusBar backgroundColor={'#b9c941'} />
            <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#b9c941'/> 
            <View style={Estilos.viewTitulo}>
                <Image source={detalheCliente} />
                <Text style={Estilos.txtTitulo}>Nossos Clientes</Text>
            </View>
            <View style={Estilos.viewCliente}>
                <Image source={cliente1} />
                <Text style={Estilos.txtCliente}>Lorem ipsum dolorem</Text>
            </View>
            <View style={Estilos.viewCliente}>
                <Image source={cliente2} />
                <Text style={Estilos.txtCliente}>Lorem ipsum dolorem</Text>
            </View>
        </View>
    );
  }
}
