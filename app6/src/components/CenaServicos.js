import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheServicos = require('../imgs/detalhe_servico.png');

const Estilos = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    txtTitulo:{
        color: '#19d1c8',
        fontSize: 30,
        marginLeft: 10
    },
    viewTitulo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    viewServicos: {
        marginTop: 20,
        padding: 20
    },
    txtServicos: {
        fontSize: 16
    }
});

export default class CenaServicos extends Component {
  render() {
    return (
        <View style={Estilos.container}>
            <StatusBar backgroundColor={'#19d1c8'} />
            <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#19d1c8'/> 
            <View style={Estilos.viewTitulo}>
                <Image source={detalheServicos} />
                <Text style={Estilos.txtTitulo}>Serviços</Text>
            </View>
            <View style={Estilos.viewServicos}>
                <Text style={Estilos.txtServicos}>. Consultoria</Text>
                <Text style={Estilos.txtServicos}>. Processos</Text>
                <Text style={Estilos.txtServicos}>. Acompanhamento de Projetos</Text>
            </View>
        </View>
    );
  }
}
