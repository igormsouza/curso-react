import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  TouchableHighlight
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const logo = require('../imgs/logo.png');
const menuCliente = require('../imgs/menu_cliente.png');
const menuContato = require('../imgs/menu_contato.png');
const menuEmpresa = require('../imgs/menu_empresa.png');
const menuServico = require('../imgs/menu_servico.png');

const Estilos = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    logo: {
        marginTop: 30, 
        alignItems: 'center'
    },
    menu: {
        margin: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    linha: {
        flexDirection: 'row'
        
    },
    imagem: {
        margin: 15
    }
});

export default class CenaPrincipal extends Component {
  render() {
    return (
        <View style={Estilos.container}>
            <StatusBar backgroundColor={'#ccc'} />
            <BarraNavegacao navigator={this.props.navigator}/> 
            <View style={Estilos.logo}>
                <Image source={logo} />
            </View>
            <View style={Estilos.menu}>
                <View style={Estilos.linha}>
                    <TouchableHighlight
                        underlayColor={'#b9c941'}
                        activeOpacity={0.3}
                        onPress={()=>{
                            this.props.navigator.push({ id: 'clientes' });
                        }}
                    >
                        <Image style={Estilos.imagem} source={menuCliente} />
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={'#61bd8c'}
                        activeOpacity={0.3}
                        onPress={()=>{
                            this.props.navigator.push({ id: 'contatos' });
                        }}
                    >
                        <Image style={Estilos.imagem} source={menuContato} />
                    </TouchableHighlight>
                </View>
                <View style={Estilos.linha}>
                    <TouchableHighlight 
                        underlayColor={'#ec7148'}
                        activeOpacity={0.3}
                        onPress={()=>{
                            this.props.navigator.push({ id: 'empresa' });
                        }}
                    >
                        <Image style={Estilos.imagem} source={menuEmpresa} />
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={'#19d1c8'}
                        activeOpacity={0.3}
                        onPress={()=>{
                            this.props.navigator.push({ id: 'servicos' });
                        }}
                    >
                        <Image style={Estilos.imagem} source={menuServico} />
                    </TouchableHighlight>
                </View>
            </View>
        </View>
    );
  }
}
