import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import Topo from './src/components/topo';
import Icone from './src/components/icone';

const Estilos = StyleSheet.create( {
  btnEscolha: {
    width: 90
  },
  painelAcoes: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10
  },
  palco: {
    alignItems: 'center'
  },
  txtResultado: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'red',
    height: 60
  }
  
});

class app4 extends Component {
  
  constructor(props) {
    super(props);

    this.state = { escolhaUsuario: '', escolhaComputador: '', resultado: ''};
  }

  jokenpo(escolhaUsuario) {
    let num = Math.floor(Math.random() * 3);
    let escolhaComputador = '';
    switch (num) {
      case 0:
        escolhaComputador = 'pedra';
        break;
      case 1:
        escolhaComputador = 'papel';
        break;
      case 2:
        escolhaComputador = 'tesoura';
        break;
      default:
        escolhaComputador = '';
    }

    let resultado = '';
    if (escolhaComputador === escolhaUsuario) {
      resultado = 'Empate!';
    } else {
      if (escolhaUsuario === 'pedra') {
        if (escolhaComputador === 'tesoura') {
          resultado = 'Você venceu!';
        } else {
          resultado = 'Você perdeu!';
        }
      }
      if (escolhaUsuario === 'papel') {
        if (escolhaComputador === 'tesoura') {
          resultado = 'Você perdeu!';
        } else {
          resultado = 'Você venceu!';
        }
      }
      if (escolhaUsuario === 'tesoura') {
        if (escolhaComputador === 'papel') {
          resultado = 'Você venceu!';
        } else {
          resultado = 'Você perdeu!';
        }
      }
    }

    //this.setState({escolhaUsuario: escolhaUsuario, escolhaComputador: escolhaComputador, resultado : resultado});
    this.setState({ escolhaUsuario, escolhaComputador, resultado });
  }

  render() {
    return (
      <View>
        <Topo/>
        <View style={ Estilos.painelAcoes }>
          <View style={ Estilos.btnEscolha }>
            <Button title="Pedra" onPress={() => { this.jokenpo('pedra'); }}/>
          </View>
          <View style={ Estilos.btnEscolha }>
            <Button title="Papel" onPress={() => { this.jokenpo('papel'); }}/>
          </View>
          <View style={ Estilos.btnEscolha }>
            <Button title="Tesoura" onPress={() => { this.jokenpo('tesoura'); }}/>
          </View>
        </View>
        <View style={ Estilos.palco }>
          <Text style={ Estilos.txtResultado }>{ this.state.resultado }</Text>
          <Icone escolha={ this.state.escolhaUsuario } jogador='Você' />
          <Icone escolha={ this.state.escolhaComputador } jogador='Computador' />
        </View>
      </View>
    );
  }
}

AppRegistry.registerComponent('app4', () => app4);
