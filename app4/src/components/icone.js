import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';

const imgPedra = require('../../imgs/pedra.png');
const imgPapel = require('../../imgs/papel.png');
const imgTesoura = require('../../imgs/tesoura.png');

class Icone extends Component {
    render() {
      if (this.props.escolha) {
        let escolha = this.props.escolha;
        let path = '';
        if (escolha === 'pedra') {
          path = imgPedra;
        } else if(escolha === 'papel') {
          path = imgPapel;
        } else {
          path = imgTesoura;
        }
        
        return (
          <View style={ Estilos.icone }>
            <Text style={ Estilos.txtJogador }>{ this.props.jogador }</Text>
            <Image source={ path }/>
          </View>
        );
      }
      
      return false;
    }
}

const Estilos = StyleSheet.create({
    icone: {
        alignItems: 'center',
        marginBottom: 20
      },
      txtJogador:{
        fontSize: 18
      }
});

export default Icone;
