import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

const Estilos = StyleSheet.create({
    numero: {
        width: 140,
        height: 80,
        fontSize: 20
    }
});

export default props => (
    <TextInput 
        style={Estilos.numero} 
        value={props.num}
        onChangeText={valor => props.atualizaValor(props.nome, valor)}        
        />
);
