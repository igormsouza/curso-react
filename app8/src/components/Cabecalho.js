import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const Cabecalho = props => (
    <View style={Estilos.topo}>
        <Text style={Estilos.txtTitulo}>Calculadora 1.0</Text>
    </View>
);

const Estilos = StyleSheet.create({
    topo: {
        backgroundColor: '#2196f3',
        padding: 10,
        alignItems: 'center'
    },
    txtTitulo: {
        fontSize: 25,
        color: 'white'
    }
});

export { Cabecalho };
