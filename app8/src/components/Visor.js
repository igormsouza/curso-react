import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

const Estilos = StyleSheet.create({
    txtVisor: {
        height: 80,
        fontSize: 18
    }
});

export default props => (
    <View > 
        <TextInput style={Estilos.txtVisor} 
            placeholder='Resultado'
            editable={false}
		    value={props.resultado}
        />
    </View>
);
