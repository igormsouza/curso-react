import React, { Component } from 'react';
import { Picker, StyleSheet } from 'react-native';

const Estilos = StyleSheet.create({
    operacao: {
        marginTop: 15,
        marginBottom: 15
    }
});

export default class Operacao extends Component {
	
	render() {
		return (
			<Picker 
				style={Estilos.operacao}
				selectedValue={this.props.operacao}	
				onValueChange={opcao => this.props.atualizaOperacao(opcao)}	
				>
				<Picker.Item label='Soma' value='soma' />
				<Picker.Item label='Subtração' value='subtracao' />
			</Picker>
		);
	}
}
