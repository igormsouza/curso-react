import React, { Component } from 'react';
import { AppRegistry, Text, View, Button } from 'react-native';
import firebase from 'firebase';

export default class firebaseTeste extends Component {

    componentWillMount() {
        var config = {
            apiKey: "AIzaSyBQLOm1nLYCeUpXS197l-Y_NjEPtB1y_Lo",
            authDomain: "projeto-teste-3c85f.firebaseapp.com",
            databaseURL: "https://projeto-teste-3c85f.firebaseio.com",
            projectId: "projeto-teste-3c85f",
            storageBucket: "projeto-teste-3c85f.appspot.com",
            messagingSenderId: "469445790946"
        };
        firebase.initializeApp(config);
    }

    cadastrarUsuario() {
        let email = "igor.souza@gmail.com";
        let senha = "123456";
        let usuario = firebase.auth();
        usuario.createUserWithEmailAndPassword(email, senha)
        .catch(
            (erro) => {
                //erro.code ou erro.message
                alert(erro.message);
            }
        );
    }

    verificaUsuarioLogado() {
        let usuario = firebase.auth();
        usuario.onAuthStateChanged(
            (usuarioAtual) => {
                if( usuarioAtual ) {
                    alert("Usuario está logado");            
                } else {
                    alert("Usuario não está logado");
                }
            }
        );
        /*let usuarioAtual = usuario.currentUser;
        if( usuarioAtual ) {
            alert("Usuario está logado");            
        } else {
            alert("Usuario não está logado");
        }*/
    }

    login() {
        let email = "igor.souza@gmail.com";
        let senha = "123456";

        let usuario = firebase.auth();
        
        usuario.signInWithEmailAndPassword(email, senha).catch(
            (error) => {
                alert(error.message);
            }
        );

    }

    logout() {
        let usuario = firebase.auth();
        usuario.signOut();
    }
    
    render() {
        return (
          <View>

            <Button 
                title="Cadastrar usuário"
                color="#841584"
                accessibilityLabel="Cadastrar usuário"
                onPress={this.cadastrarUsuario}
            />
            <Button 
                title="Verificar usuário logado"
                color="#841584"
                accessibilityLabel="Verificar usuário logado"
                onPress={this.verificaUsuarioLogado}
            />
            <Button 
                title="Login"
                color="#841584"
                accessibilityLabel="Login"
                onPress={this.login}
            />
            <Button 
                title="Logout"
                color="#841584"
                accessibilityLabel="Logout"
                onPress={this.logout}
            />
          </View>
        );
    }
}

AppRegistry.registerComponent('firebaseTeste', () => firebaseTeste);
